class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  def hithere
    render html: "Hi there, I'm here at Toy_App_1. Date: 07/06/2019. Time: 6pm."
  end

end
